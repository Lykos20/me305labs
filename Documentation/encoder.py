## @file encoder.py
#  Updates encoder position
#
#  @package encoder
#  Updates encoder position
import pyb

## An encoder driver object for motor control
class Encoder:
    
    ## Initialization
    #
    #  @param encA_pin Encoder A pin
    #  @param encB_pin Encoder B pin
    #  @param prescaler Encoder initial value
    #  @param timer Encoder timer
    #  @param period Encoder counting limit
    def __init__(self, encA_pin, encB_pin, prescaler, timer, period):
        ## Encoder A pin
        self.encA_pin = encA_pin
        ## Encoder B pin
        self.encB_pin = encB_pin
        ## Encoder initial value
        self.prescaler = prescaler
        ## Encoder timer
        self.timer = timer
        ## Encoder counting limit
        self.period = period
        ## Current position
        self.curr_position = 0
        ## Last updated position
        self.past_position = 0
        ## Distance traveled
        self.distance = 0
        ## Change between current and previous position
        self.delta = 0
    ## Updates position    
    def update(self):
        self.curr_position = self.timer.counter()
        self.distance += abs(self.delta)
        self.delta = self.current_position - self.past_position
        ## Last position is now the current position as current position gets updated
        self.last_position = self.curr_position
    ## Returns encoder's current position    
    def get_position(self):
        self.update()
        return self.curr_position
    ## Sets the encoder's position
    #
    #  @param usr_pos User input value that sets encoder's position
    def set_position(self, usr_pos):
        ## User input value that sets encoder's position
        self.usr_pos = usr_pos
        self.curr_position = self.usr_pos
        self.past_position = self.usr_pos
        self.distance = usr_pos
        self.delta = self.curr_position = self.past_position
    ## Change between current and previous position    
    def get_delta(self):
        self.update()
        if (self.delta > (self.period/2)):
            self.delta -= self.period
            self.position += self.delta
        elif (self.delta < (self.period/2)):
            self.delta += (self.period/2)
            self.position +=self.delta
        else:
            self.position += self.delta
            
        return self.delta
        