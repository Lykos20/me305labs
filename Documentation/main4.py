## @file main4.py
#  Main file for project4
#  @package main4
#  Main file for project 4

from pyb import UART

## Initializing uart
myuart = UART(2)
while True:
    if myuart.any():
        ## Value of the character uart read
        val = myuart.readchar()
        myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')
        