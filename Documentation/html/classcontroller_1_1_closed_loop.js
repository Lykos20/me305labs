var classcontroller_1_1_closed_loop =
[
    [ "__init__", "classcontroller_1_1_closed_loop.html#af399c4299720cb577125a106e4cc4f99", null ],
    [ "get_Kp", "classcontroller_1_1_closed_loop.html#a3777ff9931479e1497cc0e86422440fc", null ],
    [ "set_Kp", "classcontroller_1_1_closed_loop.html#af32f79e3ee7992f3aa8691ed02992c2b", null ],
    [ "update", "classcontroller_1_1_closed_loop.html#a28fcafa7558d7b7780cd964dcec24d52", null ],
    [ "actuator_val", "classcontroller_1_1_closed_loop.html#a5b3c4e9717ce4918315f4b5f8ccb96c8", null ],
    [ "error", "classcontroller_1_1_closed_loop.html#a0000cdd378fc90c71e8163a03040bf10", null ],
    [ "Kp", "classcontroller_1_1_closed_loop.html#aaa5ff42eb30e4e0304e80a1f2ce12b01", null ],
    [ "Kp_val", "classcontroller_1_1_closed_loop.html#a3cbeade0d914f9e4f7f8db065cfa2cc0", null ],
    [ "measured_val", "classcontroller_1_1_closed_loop.html#a363e875832bc6d0b13a0bf13146b407a", null ]
];