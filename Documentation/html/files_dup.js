var files_dup =
[
    [ "Blutooth.py", "_blutooth_8py.html", [
      [ "Blutooth", "class_blutooth_1_1_blutooth.html", "class_blutooth_1_1_blutooth" ]
    ] ],
    [ "controller.py", "controller_8py.html", [
      [ "ClosedLoop", "classcontroller_1_1_closed_loop.html", "classcontroller_1_1_closed_loop" ]
    ] ],
    [ "elevator.py", "elevator_8py.html", [
      [ "TaskElevator", "classelevator_1_1_task_elevator.html", "classelevator_1_1_task_elevator" ],
      [ "Button", "classelevator_1_1_button.html", "classelevator_1_1_button" ],
      [ "MotorDriver", "classelevator_1_1_motor_driver.html", "classelevator_1_1_motor_driver" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "Encoder", "classencoder_1_1_encoder.html", "classencoder_1_1_encoder" ]
    ] ],
    [ "lab1.py", "lab1_8py.html", "lab1_8py" ],
    [ "Lab3.py", "_lab3_8py.html", [
      [ "Encoder", "class_lab3_1_1_encoder.html", "class_lab3_1_1_encoder" ]
    ] ],
    [ "Lab4.py", "_lab4_8py.html", "_lab4_8py" ],
    [ "Lab5.py", "_lab5_8py.html", [
      [ "Light", "class_lab5_1_1_light.html", "class_lab5_1_1_light" ]
    ] ],
    [ "lab6_part1.py", "lab6__part1_8py.html", "lab6__part1_8py" ],
    [ "LED_main.py", "_l_e_d__main_8py.html", "_l_e_d__main_8py" ],
    [ "main4.py", "main4_8py.html", "main4_8py" ],
    [ "main5.py", "main5_8py.html", "main5_8py" ],
    [ "main6.py", "main6_8py.html", "main6_8py" ],
    [ "main_elevator.py", "main__elevator_8py.html", "main__elevator_8py" ],
    [ "main_encoder.py", "main__encoder_8py.html", "main__encoder_8py" ],
    [ "project4.py", "project4_8py.html", "project4_8py" ],
    [ "Task1.py", "_task1_8py.html", [
      [ "Blink", "class_task1_1_1_blink.html", "class_task1_1_1_blink" ]
    ] ],
    [ "Task2.py", "_task2_8py.html", [
      [ "Gradual", "class_task2_1_1_gradual.html", "class_task2_1_1_gradual" ]
    ] ]
];