var classlab6__part1_1_1_motor_driver =
[
    [ "__init__", "classlab6__part1_1_1_motor_driver.html#a632d57f2503d0959235e05986a829ffa", null ],
    [ "disable", "classlab6__part1_1_1_motor_driver.html#ae43f1e4d2bc5cf91aec3f516adb565ed", null ],
    [ "enable", "classlab6__part1_1_1_motor_driver.html#a4e3289e329c2ed97d66d6080385a20c2", null ],
    [ "set_duty", "classlab6__part1_1_1_motor_driver.html#a867773a8ec71aca638029f4c5f4f16b8", null ],
    [ "duty", "classlab6__part1_1_1_motor_driver.html#ad2c72cb4698a90700582300b2e868429", null ],
    [ "IN1_pin", "classlab6__part1_1_1_motor_driver.html#a8c5c852ec74c3692d6d1d475339b0b1a", null ],
    [ "IN2_pin", "classlab6__part1_1_1_motor_driver.html#a94f4e2916be171733cd3c09c0437e302", null ],
    [ "nSLEEP_pin", "classlab6__part1_1_1_motor_driver.html#a86a176f2e785831a3a802177b2d8cacf", null ],
    [ "t3ch1", "classlab6__part1_1_1_motor_driver.html#a971d4aaf7bb38aba4dea9f37eca4ef2e", null ],
    [ "t3ch2", "classlab6__part1_1_1_motor_driver.html#aa4993f0e48794f5bfde4155967975aff", null ],
    [ "timer", "classlab6__part1_1_1_motor_driver.html#ae14bbc6fa2c62904cc28dd3a54d90f8f", null ]
];