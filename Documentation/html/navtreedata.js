/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "[Tatum Yee] ME 305 Introduction to Mechatronics", "index.html", [
    [ "Introduction", "index.html#sec_intro0", null ],
    [ "Table of Contents:", "index.html#sec_ToC", null ],
    [ "Lab 1: Fibonacci", "page_lab1.html", [
      [ "What's it do?", "page_lab1.html#sec_intro1", null ],
      [ "Relevant Files", "page_lab1.html#sec_rev1", null ]
    ] ],
    [ "Lab 2: Blinking LEDs", "page_lab2.html", [
      [ "What's it do?", "page_lab2.html#sec_intro2", null ],
      [ "Task 1: Virtual LED Pattern", "page_lab2.html#sec_t21", null ],
      [ "Task 2: Physical LED Pattern", "page_lab2.html#sec_t22", null ],
      [ "Relevant Files", "page_lab2.html#sec_rev2", null ]
    ] ],
    [ "Lab 3: Incremental Encoders", "page_lab3.html", [
      [ "What's it do?", "page_lab3.html#sec_intro3", null ],
      [ "Task 1: Encoder", "page_lab3.html#sec_t31", null ],
      [ "Task 2: User Interface", "page_lab3.html#sec_t32", null ],
      [ "Relevant Files", "page_lab3.html#sec_rev3", null ]
    ] ],
    [ "Lab 4: User Interface Extended", "page_lab4.html", [
      [ "What's it do?", "page_lab4.html#sec_intro4", null ],
      [ "Task 1: UI Frontend", "page_lab4.html#sec_t41", null ],
      [ "Task 2: Data Collection Task", "page_lab4.html#sec_t42", null ],
      [ "Relevant Files", "page_lab4.html#sec_rev4", null ]
    ] ],
    [ "Lab 5: Use Your Interface", "page_lab5.html", [
      [ "What's it do?", "page_lab5.html#sec_intro5", null ],
      [ "Task Diagram Sketch", "page_lab5.html#sec_TDS", null ],
      [ "Finite State Machine Sketches", "page_lab5.html#sec_FSM", null ],
      [ "Relevant Files", "page_lab5.html#sec_rev5", null ]
    ] ],
    [ "Lab 6: DC Motors", "page_lab6.html", [
      [ "What's it do?", "page_lab6.html#sec_intro6", null ],
      [ "Relevant Files", "page_lab6.html#sec_rev6", null ]
    ] ],
    [ "Lab 7: Reference Tracking", "page_lab7.html", [
      [ "Lab 7: Reference Tracking", "page_lab7.html#sec_title7", null ],
      [ "What's it do?", "page_lab7.html#sec_intro7", null ]
    ] ],
    [ "HW 0: Finite State Machines", "page_hw0.html", [
      [ "What's it do?", "page_hw0.html#sec_intro8", null ],
      [ "State Transition Diagram for Elevator Control Algorithm", "page_hw0.html#sec_std", null ],
      [ "Relevant Files", "page_hw0.html#sec_rev0", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_blutooth_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';