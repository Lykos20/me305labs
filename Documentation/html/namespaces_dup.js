var namespaces_dup =
[
    [ "Blutooth", "namespace_blutooth.html", null ],
    [ "controller", "namespacecontroller.html", null ],
    [ "elevator", "namespaceelevator.html", null ],
    [ "encoder", "namespaceencoder.html", null ],
    [ "lab1", "namespacelab1.html", null ],
    [ "Lab3", "namespace_lab3.html", null ],
    [ "Lab4", "namespace_lab4.html", null ],
    [ "Lab5", "namespace_lab5.html", null ],
    [ "lab6_part1", "namespacelab6__part1.html", null ],
    [ "main4", "namespacemain4.html", null ],
    [ "main5", "namespacemain5.html", null ],
    [ "main6", "namespacemain6.html", null ],
    [ "main_elevator", "namespacemain__elevator.html", null ],
    [ "main_encoder", "namespacemain__encoder.html", null ],
    [ "project4", "namespaceproject4.html", null ],
    [ "Task1", "namespace_task1.html", null ],
    [ "Task2", "namespace_task2.html", null ]
];