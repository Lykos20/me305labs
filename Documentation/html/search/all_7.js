var searchData=
[
  ['get_5fdelta_30',['get_delta',['../classencoder_1_1_encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder.Encoder.get_delta()'],['../class_lab3_1_1_encoder.html#a84940c25e0ce57d88200d56cc75fcc6a',1,'Lab3.Encoder.get_delta()']]],
  ['get_5fkp_31',['get_Kp',['../classcontroller_1_1_closed_loop.html#a3777ff9931479e1497cc0e86422440fc',1,'controller::ClosedLoop']]],
  ['get_5fposition_32',['get_position',['../classencoder_1_1_encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder.Encoder.get_position()'],['../class_lab3_1_1_encoder.html#ad6859ba57bd88aa5a0a01f985b414111',1,'Lab3.Encoder.get_position()']]],
  ['getbuttonstate_33',['getButtonState',['../classelevator_1_1_button.html#a1d54234aba80648155982b165d7174b3',1,'elevator::Button']]],
  ['gobutton_34',['GoButton',['../classelevator_1_1_task_elevator.html#a3b944ae8e4f89e3cac619c0829a881ec',1,'elevator.TaskElevator.GoButton()'],['../class_lab5_1_1_light.html#a868dc5ee1c9f287a03ec5897785af2fe',1,'Lab5.Light.GoButton()'],['../class_task1_1_1_blink.html#ad96040606296a413c0df26ebe6b5ed45',1,'Task1.Blink.GoButton()'],['../_l_e_d__main_8py.html#aba7a7cc04b72ebc016db78d95252ca72',1,'LED_main.GoButton()'],['../namespacemain__elevator.html#a1d2d215e80217257479ade10dd0a09d3',1,'main_elevator.GoButton()']]],
  ['gobutton2_35',['GoButton2',['../namespacemain__elevator.html#a6b7ba75fcd866300d0e1d79e47f7440d',1,'main_elevator']]],
  ['gradual_36',['Gradual',['../class_task2_1_1_gradual.html',1,'Task2']]]
];
