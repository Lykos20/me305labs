var searchData=
[
  ['sendchar_202',['sendChar',['../namespace_lab4.html#a3c47a6950d393bce8d65d4dea50eb074',1,'Lab4']]],
  ['set_5fduty_203',['set_duty',['../classlab6__part1_1_1_motor_driver.html#a867773a8ec71aca638029f4c5f4f16b8',1,'lab6_part1::MotorDriver']]],
  ['set_5fkp_204',['set_Kp',['../classcontroller_1_1_closed_loop.html#af32f79e3ee7992f3aa8691ed02992c2b',1,'controller::ClosedLoop']]],
  ['set_5fposition_205',['set_position',['../classencoder_1_1_encoder.html#aca7cbd94d4d767579df12c05194be27a',1,'encoder.Encoder.set_position()'],['../class_lab3_1_1_encoder.html#a04bca3defa6dc371c8bbb12c676fa7b5',1,'Lab3.Encoder.set_position()']]],
  ['stop_206',['Stop',['../classelevator_1_1_motor_driver.html#aa3e2278c901886ea7f46dd552875397d',1,'elevator::MotorDriver']]]
];
