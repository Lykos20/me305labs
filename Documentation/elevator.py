## @file elevator.py
#  Finite state machine for an elevator example
#
#  @package elevator
#  This file serves as an example implementation of a finite-state-machine using
#  Python. 
#
#  The example will implement some code to control an imaginary elevator.
#  The user has two buttons to choose from - 1st floor or 2nd floor.
#  There is also a limit switch at either end of travel for the elevator.

from random import choice
import time

## A finite state machine to control an elevator between the 1st and second floor
#
#  This class implements a finite state machine to control the operation of an elevator
class TaskElevator:
    
    ## Constant defining State 0 - Initialization
    S0_INIT                       = 0
    
    ## Constant defining State 1
    S1_STOPPED_AT_FIRST_FLOOR     = 1
    
    ## Constant defining State 2
    S2_STOPPED_AT_SECOND_FLOOR    = 2
    
    ## Constant defining State 3
    S3_MOVING_DOWN                = 3
   
    ## Constant defining State 4
    S4_MOVING_UP                  = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING                 = 5
    
    ## Creates a TaskElevator object
    #
    #  @param interval Number of seconds between runs of the task
    #  @param GoButton An object from class Button representing on/off
    #  @param first An object from class Button representing the first floor switch
    #  @param second An object from class Button representing the second floor switch
    #  @param motor An object from class MotorDriver representing a DC motor.
    def __init__(self, interval, GoButton, first, second, motor):

        
        ## The state to run on the next iteration of the task.
        TaskElevator.state = self.S0_INIT
        
        ## The button object used for the go command
        self.GoButton = GoButton
        
        ## The button object used for the left limit
        self.first = first
        
        ## The button object used for the second floor limit
        self.second = second
        
        ## The motor object "lifting" and "lowering" the elevator
        self.motor = motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
    ## Runs one interation of the task    
    def run(self):
        ## Updating the current timestamp
        self.curr_time = time.time()                
        if self.curr_time > self.next_time:  # checking if the timestamp has exeeded our "scheduled" timestamp
            
            if(self.state == self.S0_INIT):
                #Run State 0 Code
                ## Button for 1st Floor
                self.button_1 = 1
                ## Button for 2nd Floor
                self.button_2 = 1
                if (self.second.getButtonState(2)):
                    self.transitionTo(self.S3_MOVING_DOWN)
                    self.motor.Down()
                else:
                    self.motor.Stop()
                print(str(self.runs) + ': State 0' + str(self.curr_time-self.start_time))
                
                
            elif(self.state == self.S1_STOPPED_AT_FIRST_FLOOR):
                # Run State 1 Code
                if (self.runs*self.interval > 10): # if our task runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                
                if (self.button_2 == 1):
                    self.transitionTo(self.S4_MOVING_UP)
                    self.motor.Up()
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))    
            
            elif(self.state == self.S2_STOPPED_AT_SECOND_FLOOR):
                # Run State 2 Code
                if  (self.button_1 == 1):
                    self.transitionTo(self.S3_MOVING_DOWN)
                    self.motor.Down()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))

            elif(self.state == self.S3_MOVING_DOWN):
                # Run State 3 Code
                if(self.first.getButtonState(1)):
                    self.transitionTo(self.S1_STOPPED_AT_FIRST_FLOOR)
                    self.motor.Stop()
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_MOVING_UP):
                # Run State 4 Code
                if (self.second.getButtonState(1)):
                    self.transitionTo(self.S2_STOPPED_AT_SECOND_FLOOR)
                    self.motor.Stop()
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S5_DO_NOTHING):
                # Run State 5 Code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1            
            
            # Specifying the next time the task will run
            self.next_time += self.interval
    ## Updates the variable defining the next state to run
    def transitionTo(self, newState):
        ## State in which elevator is transitioning to
        self.state = newState
## A pushbutton class
#  
#  This class represents a button that can be pushed by the imaginary
#  driver to go up or down to the second or first floor using an elevator.
#  As of right now, this class is implemented using "pseudo-hardware".
#  That is, we are not working with real hardware IO yet, this is all pretend.       
class Button:

    ## Creates a button object
    #
    #  @param pin   A pin object that the button is connected to
    def __init__(self, pin):

        ## The pin object used to read the Button state
        self.pin = pin
    
    ## Gets the button state
    #
    #  Since there is no hardware attached, this method returns a randomized
    #  True or False value
    #  Returns a boolean representing the state of the button
    #
    #  @param val Button "pressed"
    def getButtonState(self, val):
        #return choice([True, False])
        #self.button_1 = choice([True, False])
        # self.button_2 = choice([True, False])
        if(val == 1):
            return choice([True, False])
        else:
            return choice([True, False])
            
## A MotorDriver class
#
#  A motor driver
#
#  This class represents a motor driver used to make the elevator move up and down 
class MotorDriver:

        ## Creates a MotorDriver Object         
        def __init__(self):
            '''
            @brief Creates a MotorDriver Object
            '''
        ## Moves the motor up 
        def Up(self):
            '''
            @brief Moves the motor up
            '''
        ## Moves the motor down    
        def Down(self):
            '''
            @brief Moves the motor down
            '''
        ## Stops the motor     
        def Stop(self):
            '''
            @brief Stops the motor
            '''
        
                
                    
                    
                    
                
        
        
        