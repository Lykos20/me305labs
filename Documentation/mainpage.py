## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#  @section sec_intro0 Introduction
#  The purpose of this portfolio is to highlight all the projects worked on in
#  the Fall 2020 (what a mess of a year) ME 305 Introduction to Mechatronics
#  course. Before we get into it, the following meme depicts what I've struggled
#  with all quarter. As you shake your head chuckling at my stupidity reviewing 
#  endless lines of code, just remember this image (sorry I didn't know how to
#  make the image smaller). Below you will find the Table of Contents listing 
#  all of the labs and homeworks that were assigned.
#  @image html meme1.jpg
#  
#  @section sec_ToC Table of Contents:
#  Lab 1: Fibonacci         
#  Lab 2: Blinking LEDs          
#  Lab 3: Incremental Encoders    
#  Lab 4: User Interface Extended     
#  Lab 5: Use Your Interface     
#  Lab 6: DC Motors   
#  Lab 7: Reference Tracking   
#  HW 0: Elevator    
#    
#  @page page_lab1 Lab 1: Fibonacci
#  @section sec_intro1 What's it do?
#  The purpose of this lab was to write a program that would allow a user to 
#  input the index of the fibonacci value that they are trying to find. For example,
#  if the user inputted a value of 3, the value of 2 will be returned. (Recall
#  the fibonacci sequence: 1, 1, 2, 3, 5, 8, 13,...)
#  @section sec_rev1 Relevant Files
#  lab1.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%201/lab1.py
#
#  @page page_lab2 Lab 2: Blinking LEDs
#  @section sec_intro2 What's it do?
#  The purpose of this lab was to introduce students to MicroPython programming
#  and the Nucleo L476 development board. After setting up the hardware and the
#  software, we wrote two scripts to control the blinking of an LED. 
#  @image html nucleo.jpg
#  @section sec_t21 Task 1: Virtual LED Pattern
#  The first script was for a task in which we were turning a virtual led on and
#  off. After the program had started running, a text that either said LED OFF
#  or LED ON would be returned and until the program was interrupted, it would
#  continuing switching between the two return values.
#  @section sec_t22 Task 2: Physical LED Pattern
#  The second script was for a task in which we were changing the brightness 
#  of an LED on the nucleo dev board. The script would allow the LED to start
#  from an off state and slowly birghten until it's was at it's peak brightness
#  and then turn off. It would continue this process of gradual brightening to 
#  turning off until the program was interrupted.
#  @section sec_rev2 Relevant Files
#  LED_main.py |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/LED_main.py  
#  Task1.py    |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/Task1.py          
#  Task2.py    |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/Task2.py
#  
#  @page page_lab3 Lab 3: Incremental Encoders
#  @section sec_intro3 What's it do?
#  The purpose of this lab is to write an encoder driver class that we will 
#  implement in a future lab to measure the position of a spinning motor. This
#  project will also include a user interface for users to interact with the 
#  encoder object.
#  @section sec_t31 Task 1: Encoder
#  The encoder requires an update() method that will be called upon frequently
#  to update the various parameters of the script including current position, 
#  delta, and distance.
#  @section sec_t32 Task 2: User Interface
#  The user interface allows the user to interact with the encoder object. If
#  a 'z' is entered, the encoder position gets zeroed. 'p' will print the encoder's
#  position and 'd' will print the encoder delta. This task is constantly waiting
#  for a user input so that as soon as the user enters a character, the program
#  responds instantaneously.
#  @section sec_rev3 Relevant Files
#  Lab3.py  |   https://bitbucket.org/Lykos20/me305labs/src/master/Lab%203/Lab3.py       
#  main_encoder.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%203/main_encoder.py
#  
#  @page page_lab4 Lab 4: User Interface Extended
#  @section sec_intro4 What's it do?
#  The purpose of this lab is to create a tool that can be used to collect, plot,
#  and save data from a Nucleo-connected sensor. There are two tasks that will
#  work together to accomplish this goal. Here is a diagram of how the two tasks 
#  will work together.
#  @image html lab4taskdiagram.png
#  @section sec_t41 Task 1: UI Frontend
#  @image html frontend.png
#  The UI frontend asks for and reads a user input. If a 'g' is entered, the
#  nucleo begins samping from the encoder. The 's' stops the data collection and
#  when the data collection is complete, the script will generate a plot image
#  and a .csv file of the data, complete with timestamps and encoder readings.
#  @section sec_t42 Task 2: Data Collection Task
#  The data collection tasks samples the encoder at 5Hz for 10s until it is
#  ended by the user and stores it in an array.
#  @image html datacollectiondiagram.png
#  @section sec_rev4 Relevant Files
#  Lab4.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%204/Lab4.py     
#  main4.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%204/main4.py     
#  project4.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%204/project4.py     
#
#  @page page_lab5 Lab 5: Use Your Interface
#  @section sec_intro5 What's it do?
#  In this lab, we were introduced to the bluetooth module and using a smart
#  device to create a user interface. Communication between the smart device and
#  nucleo dev board was done using a HM-11, compatible with an iphone (iOS device)
#  We used Thunkable to build an app that would be used to blink an LED at a 
#  certain frequency.
#  @image html hm11.jpg
#  @section sec_TDS Task Diagram Sketch
#  @image html taskdiagrambluetooth.png
#  @section sec_FSM Finite State Machine Sketches
#  @image html fsmbluetooth.PNG
#  @section sec_rev5 Relevant Files
#  Blutooth.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%205/Blutooth.py     
#  Lab5.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%205/Lab5.py     
#  main5.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%205/main5.py      
#
#  @page page_lab6 Lab 6: DC Motors
#  @section sec_intro6 What's it do?
#  The purpose of this lab is to write a program that will spin some motors at
#  different speeds, utilizing PWM. The encoder will then be used to perform a
#  closed-loop speed control. 
#  @section sec_rev6 Relevant Files
#  controller.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%206/controller.py     
#  encoder.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%206/encoder.py     
#  lab6_part1.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%206/lab6_part1.py     
#  main6.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%206/main6.py     
#
#  @page page_lab7 Lab 7: Reference Tracking
#  @section sec_title7 Lab 7: Reference Tracking
#  Source code link:
#  @section sec_intro7 What's it do?
#  This lab builds off of Lab 6: DC Motors and performs reference tracking
#  instead of setpoint control.
#  
#  @page page_hw0 HW 0: Finite State Machines
#  @section sec_intro8 What's it do?
#  The purpose of this homework assignment is to learn about and implement finite
#  state machines in Python. In this particular assignment, we are building a 
#  script to control the movements of an elevator between the first and second
#  floor.
#  @section sec_std State Transition Diagram for Elevator Control Algorithm
#  @image html State_Transition_Diagram.png       
#  @section sec_rev0 Relevant Files       
#  elevator.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/HW%200/elevator.py        
#  main_elevator.py  |  https://bitbucket.org/Lykos20/me305labs/src/master/HW%200/main_elevator.py         

