## @file Lab5.py
#  Script for UI task that waits for inputs and drives LED
#  @package Lab5
#  Script for UI task that waits for inputs and drives LED

from pyb import UART
import utime
import pyb
from Blutooth import Blutooth

## This class takes a numerical value and blinks an LED at that frequency
class Light:
    
    ## State that the program starts at before GoButton is pressed
    S0_INIT = 0
    ## State that indicates the LED is ON
    S1_LED_ON = 1
    ## State that indicates the LED is OFF
    S2_LED_OFF = 2
    
    ## Initializes the different states that the LED will go through along
    #  with various time parameters.
    def __init__(self, interval, GoButton):
        
        ## Represents the current state the LED is in.
        self.state = self.S0_INIT
        
        ## Represents the button used to start the program.
        self.GoButton = GoButton
        ## Represents the Blutooth driver
        self.Blutooth = Blutooth()
    ## Runs the script that turns on and off LED at user specified frequency
    def run(self, val):
        if self.myuart.any() != 0:
            if self.uart.readchar() > 0:
                while True:
                    print('LED blinking at ' + str(self.uart.readchar()) + 'Hz')
                    self.Blutooth.ledON()
                    utime.sleep(1 / self.uart.readchar())
                    self.Blutooth.ledOFF()
                    utime.sleep(1 / self.uart.readchar())               
            else:
                self.pinA5.low()
        else:
            pass
            
        
        
    