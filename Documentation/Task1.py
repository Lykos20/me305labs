## @file Task1.py
#  Blinking LED virtually
#
#  This file serves as a virtual LED. Text will oscillate between "LED On" and "LED Off."
#
#  @package Task1
#  Implements a blinking function for a virtual LED


import utime

## Implements a blinking function for the LED
#
#  Goes back and fourth between LED ON and LED OFF
class Blink:
    ## State that the program starts at before GoButton is pressed
    S0_INIT = 0
    ## State that indicates the LED is ON
    S1_LED_ON = 1
    ## State that indicates the LED is OFF
    S2_LED_OFF = 2
    
    ## Initialization
    #  Initializes the different states that the LED will go through along
    #   with various time parameters  
    def __init__(self, interval, GoButton):
        
        ## Represents the current state the LED is in.
        self.state = self.S0_INIT
        
        ## Represents the button used to start the program.
        self.GoButton = GoButton
        
        ## Represents the number of times the program has run.
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
   
    ## Runs program
    #
    # Uses several if/else if statements to determine whether LED should be in
    # an ON or OFF state
    def run(self):    
        
        ## Represents the current time.
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                self.TransitionTo(self.S1_LED_ON)
                print(str(self.runs) + ': LED ON ')
            elif(self.state == self.S1_LED_ON):
                if(self.runs*self.interval > 3):
                    self.TransitionTo(self.S2_LED_OFF)
                    print(str(self.runs) + ': LED ON ')
            elif(self.state == self.S2_LED_OFF):
                if(self.runs*self.interval > 3):
                    self.TransitionTo(self.S1_LED_ON)
                    print(str(self.runs) + ': LED OFF ')
            else:
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    ## Transitions the LED from one state to another.       
    def TransitionTo(self, newState):

        self.state = newState
        
  

