## @file main6.py
#  Main file for lab6_part1, controller, and encoder scripts
#
#  The code to this file can be found at:
#
#
#  @package main6
#  Main file for lab6_part1, controller, and encoder scripts

import pyb

from lab6_part1 import MotorDriver
from encoder    import Encoder
from controller import ClosedLoop

## Enable pin
pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
  
## Motor input 1 used to manipulate first motor direction
pin_IN1    = pyb.Pin(pyb.Pin.cpu.B4)
## Motor input 2 used to manipulate first motor direction
pin_IN2    = pyb.Pin(pyb.Pin.cpu.B5) 
## Motor input 3 used to manipulate second motor direction
pin_IN3    = pyb.Pin(pyb.Pin.cpu.B0)
## Motor input 4 used to manipulate second motor direction
pin_IN4    = pyb.Pin(pyb.Pin.cpu.B1)

## Creates the timer object passing in the pins and timer
tim = pyb.Timer(3, freq=20000)
    
## Timer channel 1
t3ch1 = tim.channel(1, pyb.Timer.PWM, pin = pin_IN1)
## Timer channel 2
t3ch2 = tim.channel(2, pyb.Timer.PWM, pin = pin_IN2)
## Timer channel 3
t3ch3 = tim.channel(3, pyb.Timer.PWM, pin = pin_IN3)
## Timer channel 4
t3ch4 = tim.channel(4, pyb.Timer.PWM, pin = pin_IN4)

## Motor object 1
moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, t3ch1, t3ch2)
## Motor object 2
moe2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim, t3ch3, t3ch4)

## User text inputted by user
usr_txt = input('Fine a curve response? Y or N: ')
while (usr_txt == 'Y'):
    ## User input for proportional gain
    usr_kp = input('Enter a proportional gain (Kp) value: ')
    