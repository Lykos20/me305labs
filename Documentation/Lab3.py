## @file Lab3.py
#  This code will measure the position of an encoder
#
#  @package Lab3
#  This code will measure the position of an encoder



import pyb

## An encoder object
#
#  Measures position of motor 
class Encoder:
    
    ## Constructor for encoder
    #
    #  Does the setup, given appropriate parameters such as which pins and 
    #  timer to use
    def __init__(self):
        ## Encoder distance
        Encoder.distance = 0
        ## Encoder position
        Encoder.position= 0
        ## Encoder datum
        Encoder.datum = 0
        
        ## Encoder Timer
        Encoder.tim = pyb.Timer(3)
        ## Timer initialization
        self.tim.init(prescaler = 0, period = 0xFFFF)
        ## Timer channel 1
        self.tim.channel(1, pin=pyb.Pin.cpu.A6, mode = pyb.Timer.ENC_AB)
        ## Timer channel 2
        self.tim.channel(2, pin=pyb.Pin.cpu.A7, mode = pyb.Timer.ENC_AB)
    
    ## When called regularly, updates the recorded position of the encoder     
    def update(self):

        self.get_delta()
        Encoder.position += Encoder.delta
        Encoder.distance += abs(Encoder.delta)
    
    ## Gets the encoder's position
    def get_position(self):

        self.update()
        
        #print(Encoder.position)
        
    ## Resets the position to a specified value
    def set_position(self):        

        Encoder.position = 0
        
    ## Returns the difference in recorded position between the two most recent calls to update()
    def get_delta(self):
        
        Encoder.delta = (Encoder.tim.counter() - abs(Encoder.datum)) - (Encoder.position)
    
    ## Allows user to use program
    def interface(self):

        usr = input('Enter one of the following keys to interact with the encoder object \n'
                    'z (zero the encoder position \n'
                    'p (print encoder position) \n'
                    'd (print encoder delta) ')
        if (usr == 'z'):
            
        elseif (usr == 'p'):
            
        elseif (usr == 'd'):
            
        else:
            print('Please input a valid value')




        
