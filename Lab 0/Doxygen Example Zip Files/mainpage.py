## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Some information about the whole project
#
#  @section sec_lab1 Lab 1 [Fibonacci]
#  Some information about the motor driver with links. Please see motor.Motor which is part of the \ref motor package.
#
#  @section sec_lab2 Lab 2 []
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#
#  @author Tatum Yee
#
#  @copyright License Info
#
#  @date 28 January 2020
#