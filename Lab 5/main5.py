## @file main5.py
#  Main file for Lab5 and Blutooth files
#
#  @package main5
#  Main file for Lab5 and Blutooth files
    
import shares
from pyb import UART
import pyb
from Blutooth import Bluetooth
from Lab5 import Light

## User interface task, works with serial port
task = Light()

while True:
        task.run()
            
            