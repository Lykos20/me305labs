## @file Blutooth.py
#  Script for BLE module driver
#
#  @package Blutooth
#  Script for BLE module driver

from pyb import UART
import pyb

## This class serves as the blutooth command center
#
#  Driver reads from, writes to, and checks if any characters are waiting on the 
#  UART. It also allows user to turn on and off the LED.
class Blutooth:

    ## Initializes UART and LED pin
    def __init__(self):
        ## Sets uart and its baudrate
        self.myuart = UART(3, 9600)
        ## Sets LED
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    ## Reads text inputed     
    def read(self):
        val = int(self.myuart.read())
        return val
    ## Writes onto nucleo
    def writeOn(self, usrtxt):
        ## Value of the inputted text
        self.usrtxt = usrtxt
        self.myuart.write(self.usrtxt)
    ## Checks that a value has been inputted    
    def check(self):
        if self.myuart.any() != 0:
            return True
    ## Turns LED on        
    def ledON(self):
        self.pinA5.high()
    ## Turns LED off    
    def ledOFF(self):
        self.pinA5.low()
