'''
@file LED_main.py

Lab 2 Blinking Lights w/ Nucleo

Task 1 [Virtual LED Pattern] Source Code
https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/Task1.py

Task 2 [Physical LED Pattern] Source Code
https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/Task2.py

@mainpage Hi
'''

from Task1 import Blink
#from Task2 import Gradual

GoButton = True

task1 = Blink(3 , GoButton)
#task2 = Gradual()

while(True):
    task1.run()
#    task2.run()
#    task3.run()
