'''
@file main_elevator.py
@image html State_Transition_Diagram.png

'''


from elevator import Button, MotorDriver, TaskElevator
from lab1 import fib

# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
GoButton = Button('PB6')
first = Button('PB7')
second = Button('PB8')
Motor = MotorDriver()

GoButton2 = Button('PB1')
first2 = Button('PB2')
second2 = Button('PB3')
Motor2 = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskElevator(0.1, GoButton, first, second, Motor)
task2 = TaskElevator(1, GoButton2, first2, second2, Motor2)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()
#    task3.run()
