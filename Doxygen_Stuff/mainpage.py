## @file mainpage.py

#  @mainpage

#  @section sec_Intro ME 305 Documentation Portfolio

#  This documentation portfolio is for all of the homework and labs completed in 
#  ME 305 - Introduction to Mechatronics. 

#  Table of Contents
#  - @ref page_lab1
#  - @ref page_hw1
#  - @ref page_lab2
#  - @ref page_lab3
#  - @ref page_lab4
#  - @ref page_lab5
#  - @ref page_lab6

#  @author Tatum Yee
#  @date Last Updated: 17 November 2020

#  @page page_lab1 Lab 1: Fibonacci
#  The purpose of this lab is to calculate a certain digit of the fibonacci sequence.
#  The code to this file can be found at:   
#    https://bitbucket.org/Lykos20/me305labs/src/master/Lab%201/lab1.py

#  @page page_lab1 Lab 2: LEDs
#  This lab consists of two tasks running simultaneously.

#  The first task serves as a virtual LED. Text will oscillate between "LED On" and "LED Off."
#  The code to this file can be found at:
#      https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/Task1.py
  
#  The second task serves as a physical LED. The Nucleo Board LED will visibly oscillate 
#  smoothly between it's off state and on state.
#  The code to this file can be found at:    
#    https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/Task2.py


#  @page page_hw1 HW 1: Elevator
#  The purpose of this lab is to simulate an elevator going up and down between the 
#  first and second floor.
#  The code to this file can be found at:
#    https://bitbucket.org/Lykos20/me305labs/src/master/HW%200/main_elevator.py
#  And its main file source code can be found at:
