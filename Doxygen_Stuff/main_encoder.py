'''
@file main_encoder.py

The code to this file can be found at:
    
  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%203/main_encoder.py
'''

from Lab3 import Encoder

part1 = Encoder(0)
part2 = Encoder(0)

while (True):
    part1.update()
    part2.interface()