'''
@file Task2.py

The code to this file can be found at:
    
    https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/Task2.py

This file serves as a Physical LED. The Nucleo Board LED will visibly oscillate 
smoothly between it's off state and on state'
'''

import pyb

class Gradual:
    def run(self):
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        tim2 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

        x = 0
        while(True):
            while(x < 101):
                t2ch1.pulse_width_percent(x)
                x += 1
                pyb.delay(100)
            x = 0
            
            
         