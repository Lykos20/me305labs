'''
@file Lab3.py

The code to this file can be found at:
    
  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%203/Lab3.py

'''

import pyb

class Encoder:
    
    def __init__(self):
        
        '''
        @Breif    Does the setup, given appropriate parameters such as
                  which pins and timer to use
        '''
        Encoder.distance = 0
        
        Encoder.tim = pyb.Timer(3)
        self.tim.init(prescaler = 0, period = 0xFFFF)
        self.tim.channel(1, pin=pyb.Pin.cpu.A6, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pyb.Pin.cpu.A7, mode = pyb.Timer.ENC_AB)
        
    def update(self):
        
        ''' 
        @Brief    When called regularly, updates the recorded position of
                  the encoder
        '''
        Encoder.position += Encoder.delta
        Encoder.distance += Encoder.delta
        
    def get_position(self):
        
        '''
        @Brief    Returns the most recently updated position of the
                  encoder
        '''
        self.update()
        
    def set_position(self):
        
        '''
        @Brief    Resets the position to to a specified value
        '''
        
    def get_delta(self)
        
        '''
        @Brief    Returns the difference in recorded position between the
                  two most recent calls to update()
        '''
        pos1 = Encoder.tim.counter() - Encoder.datum
