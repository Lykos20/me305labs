'''
@file Task1.py

The code to this file can be found at:
    
  https://bitbucket.org/Lykos20/me305labs/src/master/Lab%202/Task1.py

This file serves as a virtual LED. Text will oscillate between "LED On" and "LED Off."
'''


import time

class Blink:
    S0_INIT = 0
    S1_LED_ON = 1
    S2_LED_OFF = 2
    def __init__(self, interval, GoButton):
        
        self.state = self.S0_INIT
        self.GoButton = GoButton
        
        self.runs = 0
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
    def run(self):
        
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            if(self.state == self.S0_INIT):
                self.TransitionTo(self.S1_LED_ON)
                print(str(self.runs) + ': LED ON ' + str(self.curr_time-self.start_time))
            elif(self.state == self.S1_LED_ON):
                if(self.runs*self.interval > 3):
                    self.TransitionTo(self.S2_LED_OFF)
                    print(str(self.runs) + ': LED ON ' + str(self.curr_time-self.start_time))
            elif(self.state == self.S2_LED_OFF):
                if(self.runs*self.interval > 3):
                    self.TransitionTo(self.S1_LED_ON)
                    print(str(self.runs) + ': LED OFF ' + str(self.curr_time-self.start_time))
            else:
                pass
            
            self.runs += 1
            self.next_time = self.next_time + self.interval
            
    def TransitionTo(self, newState):
        self.state = newState
        
  

