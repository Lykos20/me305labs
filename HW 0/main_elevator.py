## @file main_elevator.py
#  Main file that runs elevator.py script
#  @package main_elevator
#  Main file that runs elevator.py script

from elevator import Button, MotorDriver, TaskElevator
#from lab1 import fib

# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
## Button to start program for first elevator
GoButton = Button('PB6')
## Button for first floor for first elevator
first = Button('PB7')
## Button for second floor for first elevator
second = Button('PB8')
## Motor Driver for first elevator
Motor = MotorDriver()

## Button to start program for second elevator
GoButton2 = Button('PB1')
## Button for first floor for second elevator
first2 = Button('PB2')
## Button for second floor for second elevator
second2 = Button('PB3')
## Motor Driver for second elevator
Motor2 = MotorDriver()

# Creating a task object using the button and motor objects above
## Task for first elevator
task1 = TaskElevator(0.1, GoButton, first, second, Motor)
## Task for second elevator
task2 = TaskElevator(1, GoButton2, first2, second2, Motor2)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()
#    task3.run()
