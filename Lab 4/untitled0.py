# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 10:36:24 2020

@author: Lykos
"""

import pyb
import serial
import UART
import utime
import keyboard

ser = serial.Serial(port='COM3', baudrate=115273, timeout=1)
myuart = UART(2)

pos = []

class Encoder:
    
    def __init__(self):
        
        Encoder.position = 0
        
        Encoder.tim = pyb.Timer(3)
        self.tim.init(prescaler = 0, period = 0xFFFF)
        self.tim.channel(1, pin=pyb.Pin.cpu.A6, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pyb.Pin.cpu.A7, mode = pyb.Timer.ENC_AB)
    
    def interface(self):
        inv = input('Type G to start recording: ')
        ser.write(str(inv).encode('ascii'))
        myval = ser.readline().decode('ascii')
        print(myval)
        
        if str(myval) == 71:
            print(Encoder.tim.counter())
            t = utime.time()
            print("Recording has started. Enter S to stop recording")
            while True:
                pos.append(Encoder.position)
                if (keyboard.is_pressed('S')) or ((utime.ticks_diff(utime.time()),t) > 15):
                    print("Recording Stopped")
                    print(pos)