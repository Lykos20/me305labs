## @file project4.py
#  Extension of Lab3 encoder interface
#
#  @package project4
#  Extension of Lab3 encoder interface


import pyb
from pyb import UART
import utime
import keyboard

## Initializing serial
ser = serial.Serial(port='COM3', baudrate=115273, timeout=1)
## Initializing uart
myuart = UART(2)

## Array with the values of the positions of encoder
pos = []

## An encoder driver object
class Encoder:
    ## Initializes various parameters
    def __init__(self):
        
        Encoder.position = 0
        
        Encoder.tim = pyb.Timer(3)
        self.tim.init(prescaler = 0, period = 0xFFFF)
        self.tim.channel(1, pin=pyb.Pin.cpu.A6, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2, pin=pyb.Pin.cpu.A7, mode = pyb.Timer.ENC_AB)
    
    ## Creates interface
    def interface(self):
        inv = input('Type G to start recording: ')
        myuart.write(str(inv).encode('ascii'))
        myval = myuart.readline().decode('ascii')
        print(myval)
        
        if str(myval) == 71:
            print(Encoder.tim.counter())
            t = utime.time()
            print("Recording has started. Enter S to stop recording")
            while True:
                pos.append(Encoder.position)
                if (keyboard.is_pressed('S')) or ((utime.ticks_diff(utime.time()),t) > 15):
                    print("Recording Stopped")
                    print(pos)

        
        
            
            