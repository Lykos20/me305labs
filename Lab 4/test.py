# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 22:55:42 2020

@author: Lykos
"""

import pyb
import UART
#import time
    
class Encoder:
    
    def __init__(self):
        myuart = UART(2)
        
        tim = pyb.Timer(3)
        tim.init(prescaler = 0, period = 0xFFFF)
        tim.channel(1, pin=pyb.Pin.cpu.A6, mode = pyb.Timer.ENC_AB)
        tim.channel(2, pin=pyb.Pin.cpu.A7, mode = pyb.Timer.ENC_AB)

        #while (True):
         #   print(tim.counter())
    
    def interface(self):
        inv = input('Type G to start recording: ')
        myuart.write(str(inv).encode('ascii'))
        myval = myuart.readline().decode('ascii')
        print(myval)
        
        if (str(myval) == 71):
            print(Encoder.tim.counter())
            #t = time.time()
            print("Recording has started. Enter S to stop recording")
            '''
            while True:
                pos.append(Encoder.position)
                if (keyboard.is_pressed('S')) or ((time.time() - t) > 15):
                    print("Recording Stopped")
                    print(pos)
                    '''
