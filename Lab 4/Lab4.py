## @file Lab4.py
#  User sends in a character and its ascii value is outputted
#
#  @package Lab4
#  User sends in a character and its ascii value is outputted



import serial
## Initializing serial port
ser = serial.Serial(port='COM3', baudrate=115273, timeout=1)

## Requests a character and prints out the ASCCI equivalent
def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    print (myval)

for n in range(3):
    sendChar()
    
ser.close()
