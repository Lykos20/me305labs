## @file lab1.py
#  This code will return the fibonacci value of a specified index of the series
#
#  @package lab1
#  This code will return the fibonacci value of a specified index of the series

## This method calculates a Fibonacci number correspondng to a specified index.
#  Continues interating until the value of the fibonacchi number is found
#
#  @param idx An integer specifying the index of the desired Fibonacci number.
def fib (idx):
    
    series = [0, 1]
    if (idx) < 0:
        print('Please enter a numerical index value GREATER THAN OR EQUAL TO ZERO')
    elif (idx) < 2:
        print('Fibonnaci number at index {} is {}.'.format(idx, series[idx]))
    else:
        x = int(2)
        while x <= idx:
            series.append(series[x-2] + series[x-1])
            x += 1
        print ('Fibonnaci number at index {} is {}.'.format(idx, series[idx]))
        #print (fib(idx - 1) + fib(idx - 2))
        #''' print('Calculating Fibonacci number at' 'index n = {:}.'.format(idx))'''
   
if __name__=='__main__':
    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonnaci function. Any code
    # within the if __name__=='__main__' block will only run when 
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    
    ## Index at which user is searching for the fibonacci value
    num = int(input('Please enter the index of the desired Fibonacci number: '))
    ## Represents the value of the fibonacci sequence at the digit specified by 'num'
    fib(num)
    
    
    