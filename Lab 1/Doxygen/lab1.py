# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 09:44:12 2020

@author: Lykos
"""

def fib (idx):
    '''This method calculates a Fibonnachi number correspondng to a specified index.
    @param idx An integer specifying the index of the desired Fibonacci number.'''
    
    series = [0, 1]
    if (idx) < 0:
        print('Please enter a numerical index value GREATER THAN OR EQUAL TO ZERO')
    elif (idx) < 2:
        print('Fibonnaci number at index {} is {}.'.format(idx, series[idx]))
    else:
        x = int(2)
        while x <= idx:
            series.append(series[x-2] + series[x-1])
            x += 1
        print ('Fibonnaci number at index {} is {}.'.format(idx, series[idx]))
        #print (fib(idx - 1) + fib(idx - 2))
        #''' print('Calculating Fibonacci number at' 'index n = {:}.'.format(idx))'''
   
if __name__=='__main__':
    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonnaci function. Any code
    # within the if __name__=='__main__' block will only run when 
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    num = int(input('Please enter the index of the desired Fibonacci number: '))
    fib(num)
    