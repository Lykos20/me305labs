## @file lab6_part1.py
#  Runs the motors
#  @package lab6_part1
#  Runs the motors

import pyb
## This class implements a motor driver for the ME405 board.
class MotorDriver:
    
    ## Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
    #
    #  @param nSLEEP_pin    A pyb.Pin object to use as the enable pin.
    #  @param IN1_pin       A pyb.Pin object to use as the input to half bridge 1.
    #  @param IN2_pin       A pyb.Pin object to use as the input to half bridge 2.
    #  @param timer         A pyb.Pin object to use for PWM generation on IN1_pin 
    #                       and IN2_pin.
    #  @param t3ch1         A tim.channel object used to set channel 1
    #  @param t3ch2         A tim.channel object used to set channel 2    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer, t3ch1, t3ch2):

        ## A pyb.Pin object to use as the enable pin.
        self.nSLEEP_pin = nSLEEP_pin
        ## A pyb.Pin object to use as the input to half bridge 1.
        self.IN1_pin = IN1_pin
        ## A pyb.Pin object to use as the input to half bridge 2.
        self.IN2_pin = IN2_pin

        ## A pyb.Pin object to use for PWM generation on IN1_pin 
        #  and IN2_pin.
        self.timer = timer
        ## A tim.channel object used to set channel 1
        self.t3ch1 = t3ch1
        ## A tim.channel object used to set channel 2
        self.t3ch2 = t3ch2
        
        print('Creating a motor driver')
    
    ## Enables motor
    def enable(self):
        
        self.nSLEEP_pin.high()
        
        print('Enabling Motor')
        
    ## Disables motor   
    def disable(self):
        
        self.nSLEEP_pin.low()
        
        print('Disabling Motor')
    ## Sets duty cycle to be sent to motor to given level  
    #
    #  Positive values cause effort in one direction, negative values
    #  in the opposite direction.
    #
    #  @param duty A signed integer holding the duty cycle of the PWM signal 
    #              sent to the motor.
    def set_duty(self, duty):
        ## Sets duty cycle
        self.duty = duty
        
        if (self.duty > 0): # Reverse: In1 (0) In2 (1)
            #self.IN1_pin.low()
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(self.duty)
            print('Duty cycle: ' + str(self.duty))
            
        elif (self.duty < 0): # Forwards: In1 (1) In2 (0)
            self.t3ch2.pulse_width_percent(0)
            self.t3ch1.pulse_width_percent(abs(self.duty))
            #self.IN2_pin.low()
            print('Duty cycle: ' + str(self.duty))
            
        else:
            pass
        
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacting with the motor driver
    ## Enable pin
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    
    ## Motor input 1 used to manipulate first motor direction
    pin_IN1    = pyb.Pin(pyb.Pin.cpu.B4)
    ## Motor input 2 used to manipulate first motor direction
    pin_IN2    = pyb.Pin(pyb.Pin.cpu.B5) 
    ## Motor input 3 used to manipulate second motor direction
    pin_IN3    = pyb.Pin(pyb.Pin.cpu.B0)
    ## Motor input 4 used to manipulate second motor direction
    pin_IN4    = pyb.Pin(pyb.Pin.cpu.B1)
    
    ## Creates the timer object passing in the pins and timer
    tim = pyb.Timer(3, freq=20000)
    
    ## Timer channel 1
    t3ch1 = tim.channel(1, pyb.Timer.PWM, pin = pin_IN1)
    ## Timer channel 2
    t3ch2 = tim.channel(2, pyb.Timer.PWM, pin = pin_IN2)
    ## Timer channel 3
    t3ch3 = tim.channel(3, pyb.Timer.PWM, pin = pin_IN3)
    ## Timer channel 4
    t3ch4 = tim.channel(4, pyb.Timer.PWM, pin = pin_IN4)

    
    # Create a motor object passing in the pins and timer
    ## Motor object 1
    moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, t3ch1, t3ch2)
    ## Motor object 2
    moe2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim, t3ch3, t3ch4)
    
    # Enable the motor driver
    ## Motor driver 1
    moe1.enable()
    ## Motor driver 2
    moe2.enable()
    
    # Set the duty cycle to 10 percent
    ## Duty cycle 1
    moe1.set_duty(0)
    ## Duty cycle 2
    moe2.set_duty(0)
        
        