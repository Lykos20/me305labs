## @file controller.py
#  Regulates motor response
#
#  @package controller
#  Regulates motor response

import pyb

## Creates a closed loop controller that will correct outputs in a continuous 
#  feedback loop so that output is as accurate as possible.
class ClosedLoop:
    ## Initializes gain and saturation limit
    #
    #  @param Kp The proportional controller gain
    #  @param sat_lim The saturation limit
    def __init__(self, Kp, sat_lim):
        ## Proportional controller gain
        self.Kp = Kp
        ## Saturation limit
        self.sat_lim
    ## Updates the actuator value 
    #
    # @param measured_val Used to help correct output
    def update(self, measured_val):
        ## Gets measured value
        self.measured_val = measured_val
        ## Gets error
        self.error = self.sat_lim - self.measured_val
        ## Error correction
        self.actuator_val = self.Kp * self.error
        
        return self.actuator_val
    ## Gets Kp value
    def get_Kp(self):
        self.update()
        return self.Kp
    ## Sets Kp value
    #
    # @param Kp_val Gets Kp_val
    def set_Kp(self, Kp_val):
        ## Gets Kp_val
        self.Kp_val = Kp_val
        self.Kp = self.Kp_val
        
        
        