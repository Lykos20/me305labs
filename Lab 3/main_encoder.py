## @file main_encoder.py
#  Main file for Lab3.py Encoder file
#
#  @package main_encoder 
#  Main file for Lab3.py Encoder file


from Lab3 import Encoder

## Variable for Encoder 1
part1 = Encoder(0)
## Variable for Encoder 2
part2 = Encoder(0)

while (True):
    part1.update()
    part2.interface()