## @file Task2.py
#  Physical blinking LED
#  
#  This file serves as a Physical LED. The Nucleo Board LED will visibly oscillate 
#  smoothly between it's off state and on state'
#
#  @package Task2
#  Physical blinking LED


import pyb

## This class implements function for the LED to gradually brighten and dim
class Gradual:
    ## Runs the program that allows gradual brightening and dimming of LED
    def run(self):
        ## LED
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        ## Timer
        tim2 = pyb.Timer(2, freq = 20000)
        ## Channel
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        ## Counter
        x = 0
        while(True):
            while(x < 101):
                t2ch1.pulse_width_percent(x)
                x += 1
                pyb.delay(100)
            x = 0
            
            
         