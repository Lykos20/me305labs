## @file LED_main.py
#  Lab 2 blinks an LED on a Nucleo Dev Board



from Task1 import Blink
from Task2 import Gradual
from pyb   import UART

## Initializes UART
myuart = UART(2)

## The button that starts the program
GoButton = True

## Task 1 is for the vitural LED pattern
task1 = Blink(3 , GoButton)
## Task 2 is for the physical LED patter
task2 = Gradual()
       
while True:
    task1.run()
    task2.run()
#    task3.run()
